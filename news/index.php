<?php
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");

$APPLICATION->SetPageProperty('title', 'News & Media');
$APPLICATION->SetTitle('Our Blog');

$APPLICATION->IncludeComponent(
    'bitrix:news',
    'blog',
    [
        'IBLOCK_TYPE'       => 'content',
        'IBLOCK_ID'         => get_iblock_id_by_code('blog'),
        'NEWS_COUNT'        => 3,
        'PAGER_TEMPLATE'    => 'blog_pagination',
        'SEF_MODE'          => 'Y',
        'SEF_FOLDER'        => '/news/',
        'SEF_URL_TEMPLATES' => [
            'section' => '#SECTION_CODE#/',
            'detail'  => '#SECTION_CODE_PATH#/#ELEMENT_CODE#/',
        ],
        'BROWSER_TITLE'     => 'NAME',
        'FIELD_CODE'        => ['TAGS'],
        'DETAIL_FIELD_CODE' => ['TAGS'],
        'LIST_FIELD_CODE'   => ['TAGS'],
        'SHOW_TAGS'         => 'Y',
        'INCLUDE_IBLOCK_INTO_CHAIN' => 'N',
        'SET_TITLE'         => 'Y',
        'SET_STATUS_404'    => 'Y',
        'SHOW_404'          => 'Y',
    ],
);

require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");