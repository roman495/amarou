<?php 
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");

echo 'TEST';

$APPLICATION->IncludeComponent('bitrix:menu', 'top_menu', [
    'ROOT_MENU_TYPE'        => 'top',
    'MAX_LEVEL'             => '2',
    'CHILD_MENU_TYPE'       => 'section',
    'USE_EXT'               => 'Y',
    'DELAY'                 => 'N',
    'ALLOW_MULTI_SELECT'    => 'Y',
    'MENU_CACHE_TYPE'       => 'N',
    'MENU_CACHE_TIME'       => '3600',
    'MENU_CACHE_USE_GROUPS' => 'Y',
    'MENU_CACHE_GET_VARS'   => ''
]);

