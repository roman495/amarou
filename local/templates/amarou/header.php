<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<!doctype html>
<html>
<head>
    <?php
    $APPLICATION->ShowHead();

    use Bitrix\Main\Page\Asset;

    Asset::getInstance()->addCss('https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.3/css/all.min.css');
    Asset::getInstance()->addCss(SITE_TEMPLATE_PATH . '/assets/css/libraries.css');
    Asset::getInstance()->addCss(SITE_TEMPLATE_PATH . '/assets/css/style.css');
    
    Asset::getInstance()->addJs(SITE_TEMPLATE_PATH . '/assets/js/jquery-3.5.1.min.js');
    Asset::getInstance()->addJs(SITE_TEMPLATE_PATH . '/assets/js/plugins.js');
    Asset::getInstance()->addJs(SITE_TEMPLATE_PATH . '/assets/js/main.js');
  
    Asset::getInstance()->addString("<link rel='shortcut icon' href='<?=SITE_TEMPLATE_PATH?>/assets/images/favicon/favicon.png' />");
    Asset::getInstance()->addString("<meta name='viewport' content='width=device-width, initial-scale=1'>");
    Asset::getInstance()->addString('<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Rubik:400,500,600,700%7cRoboto:400,500,700&display=swap">');
    ?>
	<title><?$APPLICATION->ShowTitle()?></title>
</head>
<body>
<?$APPLICATION->ShowPanel();?>

<header>
<div class="wrapper">
    <!-- <div class="preloader">
      <div class="spinner"><span class="cube1"></span><span class="cube2"></span></div>
    </div> -->
    <!-- /.preloader -->

    <!-- =========================
        Header
    =========================== -->
    <header class="header header-light header-layout1">
      <div class="header-topbar d-none d-xl-block">
        <div class="container-fluid">
          <div class="row">
            <div class="col-sm-12 col-md-7 col-lg-7">
              <div class="d-flex flex-wrap justify-content-between">
                <ul class="contact-list list-unstyled mb-0 d-flex flex-wrap">
                  <li>
                    <i class="icon-phone"></i><span>Phone:</span>
                    <a href="tel:556554117">+55 654 541 17</a>
                  </li>
                  <li>
                    <i class="icon-envelope"></i><span>Email:</span>
                    <a href="mailto:Amarou@7oroof.com">Amarou@7oroof.com</a>
                  </li>
                  <li><i class="icon-clock"></i><span>Hours: Mon-Fri: 8am – 7pm</span></li>
                </ul>
              </div>
            </div><!-- /.col-lg-7 -->
            <div class="col-sm-12 col-md-5 col-lg-5 d-flex flex-wrap justify-content-end">
              <ul class="header-topbar__links d-flex flex-wrap list-unstyled mb-0">
                <li><a href="#">New & Media</a></li>
                <li><a href="contacs.html">Contacts</a></li>
                <li><a href="careers.html">Careers</a></li>
              </ul>
              <ul class="social-icons list-unstyled mb-0">
                <li><a href="#"><i class="fab fa-facebook-f"></i></a></li>
                <li><a href="#"><i class="fab fa-instagram"></i></a></li>
                <li><a href="#"><i class="fab fa-twitter"></i></a></li>
              </ul>
            </div><!-- /.col-lg-5 -->
          </div><!-- /.row -->
        </div><!-- /.container -->
      </div><!-- /.header-topbar -->
      <nav class="navbar navbar-expand-lg">
        <div class="container-fluid px-0">
          <a class="navbar-brand" href="/">
            <img src="<?=SITE_TEMPLATE_PATH?>/assets/images/logo/logo-dark.png" class="logo-dark" alt="logo">
          </a>
          <button class="navbar-toggler" type="button">
            <span class="menu-lines"><span></span></span>
          </button>
            <?php               
                $APPLICATION->IncludeComponent('bitrix:menu', 'top_menu', [
                    'ROOT_MENU_TYPE'        => 'top',
                    'MAX_LEVEL'             => '2',
                    'CHILD_MENU_TYPE'       => 'section',
                    'USE_EXT'               => 'Y',
                    'DELAY'                 => 'N',
                    'ALLOW_MULTI_SELECT'    => 'Y',
                    'MENU_CACHE_TYPE'       => 'N',
                    'MENU_CACHE_TIME'       => '3600',
                    'MENU_CACHE_USE_GROUPS' => 'Y',
                    'MENU_CACHE_GET_VARS'   => ''
                ]);
            ?>
          <div class="header-actions d-flex align-items-center">
            <button type="button" class="action-btn__search"><i class="fas fa-search"></i></button>
            <a href="request-quote.html" class="btn btn__primary action-btn__request">
              <span>Request A Quote</span><i class="icon-arrow-right"></i>
            </a>
          </div>
        </div><!-- /.container -->
      </nav><!-- /.navabr -->
    </header><!-- /.Header -->

    <!-- Хлебные крошки (навигация) -->
  <?php if ($APPLICATION->GetCurPage() != '/') {
      $APPLICATION->IncludeComponent(
          'bitrix:breadcrumb',
          '',
          [
              'START_FROM' => '0',
              'SITE_ID'    => 's1',
          ]
      );
  } ?>
