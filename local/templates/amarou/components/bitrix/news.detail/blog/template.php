<?php
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

use \Bitrix\Main\Localization\Loc;
Loc::loadLanguageFile(__FILE__);
?>
<section class="blog blog-single mt-3 pt-0 pb-40">
    <div class="container">
        <div class="row">
            <div class="col-sm-12 col-md-12 col-lg-4">
                <aside class="sidebar mb-30">
                    <div class="widget widget-search">
                    <h5 class="widget-title">Search</h5>
                    <div class="widget-content">
                        <form class="widget-form__search">
                        <input type="text" class="form-control" placeholder="Search...">
                        <button class="btn" type="submit"><i class="fa fa-search"></i></button>
                        </form>
                    </div><!-- /.widget-content -->
                    </div><!-- /.widget-search -->
                    <div class="widget widget-posts">
                    <h5 class="widget-title">Recent Posts</h5>
                    <div class="widget-content">
                        <div class="slick-carousel" data-slick='{"slidesToShow": 1, "arrows": false, "dots": true}'>
                        <!-- post item #1 -->
                        <div class="widget-post-item">
                            <div class="widget-post__img">
                            <a href="#"><img src="assets/images/blog/grid/1.jpg" alt="thumb"></a>
                            </div><!-- /.widget-post-img -->
                            <div class="widget-post__content">
                            <span class="widget-post__date">Jan 20, 2020</span>
                            <h6 class="widget-post__title"><a href="#">New Additions to our great Metro trucks Fleet</a>
                            </h6>
                            </div><!-- /.widget-post-content -->
                        </div><!-- /.widget-post-item -->
                        <!-- post item #2 -->
                        <div class="widget-post-item">
                            <div class="widget-post__img">
                            <a href="#"><img src="assets/images/blog/grid/2.jpg" alt="thumb"></a>
                            </div><!-- /.widget-post-img -->
                            <div class="widget-post__content">
                            <span class="widget-post__date">Jan 23, 2020</span>
                            <h6 class="widget-post__title"><a href="#">Cargo flow through better supply chain visibility,
                                control.</a></h6>
                            </div><!-- /.widget-post-content -->
                        </div><!-- /.widget-post-item -->
                        </div><!-- /.carousel -->
                    </div><!-- /.widget-content -->
                    </div><!-- /.widget-posts -->
                    <div class="widget widget-tags">
                    <h5 class="widget-title">Tags</h5>
                    <div class="widget-content">
                        <ul class="list-unstyled">
                        <li><a href="#">Insights</a></li>
                        <li><a href="#">Industry</a></li>
                        <li><a href="#">Modern</a></li>
                        <li><a href="#">Corporate</a></li>
                        <li><a href="#">Business</a></li>
                        </ul>
                    </div><!-- /.widget-content -->
                    </div><!-- /.widget-tags -->
                </aside><!-- /.sidebar -->
            </div><!-- /.col-lg-4 -->
            <div class="col-sm-12 col-md-12 col-lg-8">
                <div class="post-item mb-0">
                    <div class="post-item__img">
                        <a href="#">
                            <img src="<?= $arResult['COVER'] ?>" alt="blog image">
                        </a>
                    </div><!-- /.entry-img -->
                    <div class="post-item__content">
                        <div class="post-item__meta d-flex align-items-center">
                            <div class="post-item__meta-cat">
                                <?php foreach($arResult['categories'] as $cat) : ?>
                                    <a href="<?= $cat['url'] ?>"><?= $cat['title'] ?></a>
                                <?php endforeach ?>
                            </div><!-- /.blog-meta-cat -->
                            <span class="post-item__meta-date"><?= $arResult['DATE'] ?></span>
                            <span class="post-item__meta-author">By: <a href="#" style="text-transform:capitalize"><?= $arResult['AUTHOR'] ?></a></span>
                        </div><!-- /.blog-meta -->
                        <h1 class="post-item__title"><?= $arResult['NAME'] ?></h1>
                        <div class="post-item__desc"><?= $arResult['DETAIL_TEXT'] ?></div><!-- /.blog-desc -->
                    </div><!-- /.entry-content -->
                </div><!-- /.post-item -->
                <div class="blog-share d-flex flex-wrap justify-content-between mb-30">
                    <a href="#" class="btn btn__social btn__social__lg btn__secondary">
                    <i class="fab fa-facebook-f"></i><span>Share on Facebook</span>
                    </a>
                    <a href="#" class="btn btn__social btn__social__lg btn__secondary">
                    <i class="fab fa-twitter"></i><span>Share on Twitter</span>
                    </a>
                    <a href="#" class="btn btn__social btn__social__lg btn__secondary">
                    <i class="fab fa-google"></i><span>Share on Google</span>
                    </a>
                </div><!-- /.blog-share -->
                <div class="blog-tags d-flex flex-wrap">
                    <strong class="mr-20 color-heading">Tags:</strong>
                    <ul class="list-unstyled d-flex flex-wrap mb-40">
                        <?php foreach($arResult['tags'] as $tag) : ?>
                            <li><a href="/search?tags=<?= $tag ?>"><?= $tag ?></a></li>
                        <?php endforeach ?>
                    </ul>
                </div><!-- /.blog-tags -->
                <div class="blog-widget blog-nav mb-0">
                    <div class="post-item__prev">
                    <a href="#">
                        <div class="post-item__nav-img">
                        <img src="assets/images/blog/grid/4.jpg" alt="blog thumb">
                        </div>
                        <div class="post-item__nav-content">
                        <span>Previous</span>
                        <h5>Importers achieve cost savings through the First Sale rule!</h5>
                        </div>
                    </a>
                    </div><!-- /.blog-prev  -->
                    <div class="post-item__next">
                    <a href="#">
                        <div class="post-item__nav-content">
                        <span>Next</span>
                        <h5>Cargo flow through better supply chain visibility, control.</h5>
                        </div>
                        <div class="post-item__nav-img">
                        <img src="assets/images/blog/grid/6.jpg" alt="blog thumb">
                        </div>
                    </a>
                    </div><!-- /.blog-next  -->
                </div><!-- /.blog-nav  -->
                <div class="blog-widget">
                    <div class="blog-author">
                    <div class="post-item__author-avatar">
                        <img src="assets/images/blog/author/1.jpg" alt="avatar">
                    </div><!-- /.author-avatar  -->
                    <div class="post-item__author-content">
                        <h6 class="post-item__author-name">Mahmoud Baghagho</h6>
                        <p class="post-item__author-bio">Founded by Begha over many cups of tea at her kitchen table in 2009,
                        our
                        brand promise is simple: to
                        provide powerful digital marketing solutions to small and medium businesses.</p>
                        <ul class="social-icons list-unstyled mb-0">
                        <li><a href="#"><i class="fab fa-facebook-f"></i></a></li>
                        <li><a href="#"><i class="fab fa-twitter"></i></a></li>
                        <li><a href="#"><i class="fab fa-vimeo-v"></i></a> </li>
                        <li><a href="#"><i class="fab fa-linkedin"></i></a> </li>
                        </ul>
                    </div><!-- /.author-content  -->
                    </div><!-- /.blog-widget  -->
                </div><!-- /.blog-author  -->
                <div class="blog-comments">
                    <h5 class="post-item__widget-title">2 comments</h5>
                    <ul class="comments-list list-unstyled">
                    <li class="comment-item">
                        <div class="comment-item__avatar">
                        <img src="assets/images/blog/author/2.png" alt="avatar">
                        </div>
                        <div class="comment-item__content">
                        <h6 class="comment-item__author">Richard Muldoone</h6>
                        <span class="comment-item__date">Feb 28, 2017 - 08:07 pm</span>
                        <p class="comment-item__desc">The example about the mattress sizing page you mentioned in the last
                            WBF
                            can be a perfect example
                            of new keywords and content, and broadening the funnel as well. I can only imagine the sale
                            numbers if that was the site of a mattress selling company.</p>
                        <a class="comment-item__reply" href="#">reply</a>
                        </div>
                        <ul class="nested-comment list-unstyled">
                        <li class="comment-item">
                            <div class="comment-item__avatar">
                            <img src="assets/images/blog/author/3.png" alt="avatar">
                            </div>
                            <div class="comment-item__content">
                            <h6 class="comment-item__author">Mike Dooley</h6>
                            <span class="comment-item__date">Feb 28, 2017 - 08:22 pm</span>
                            <p class="comment-item__desc">The example about the mattress sizing page you mentioned in the
                                last
                                WBF can be a perfect
                                example of new keywords and content, and broadening the funnel as well. I can only imagine the
                                sale numbers if that was the site of a mattress selling company.</p>
                            <a class="comment-item__reply" href="#">reply</a>
                            </div>
                        </li><!-- /.comment -->
                        </ul><!-- /.nested-comment -->
                    </li><!-- /.comment -->
                    </ul><!-- /.comments-list -->
                </div><!-- /.blog-comments -->
                <div class="blog-widget blog-comments-form">
                    <h5 class="post-item__widget-title">Leave A Reply</h5>
                    <form method="post" action="assets/php/contact.php" id="contactForm">
                    <div class="row">
                        <div class="col-sm-12 col-md-4 col-lg-4">
                        <div class="form-group">
                            <input type="text" class="form-control" placeholder="Name:" id="contact-name" name="contact-name"
                            required>
                        </div><!-- /.form-group -->
                        </div><!-- /.col-lg-6 -->
                        <div class="col-sm-12 col-md-4 col-lg-4">
                        <div class="form-group">
                            <input type="email" class="form-control" placeholder="Email:" id="contact-email"
                            name="contact-email" required>
                        </div><!-- /.form-group -->
                        </div><!-- /.col-lg-6 -->
                        <div class="col-sm-12 col-md-4 col-lg-4">
                        <div class="form-group">
                            <input type="text" class="form-control" placeholder="Phone" id="contact-Phone"
                            name="contact-phone" required>
                        </div><!-- /.form-group -->
                        </div><!-- /.col-lg-6 -->
                        <div class="col-sm-12 col-md-12 col-lg-12">
                        <div class="form-group">
                            <textarea class="form-control" placeholder="Comment" id="contact-messgae" name="contact-messgae"
                            required></textarea>
                        </div><!-- /.form-group -->
                        </div><!-- /.col-lg-12 -->
                        <div class="col-sm-12 col-md-12 col-lg-12">
                        <button type="submit" class="btn btn__secondary btn__lg">
                            <i class="icon-arrow-right"></i><span>Submit Comment</span>
                        </button>
                        <div class="contact-result"></div>
                        </div><!-- /.col-lg-12 -->
                    </div><!-- /.row -->
                    </form>
                </div><!-- /.blog-comments-form -->
            </div><!-- /.col-lg-8 -->
        </div><!-- /.row -->
    </div><!-- /.container -->
</section><!-- /.blog Single -->

