<?php
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)
    die();
    
if($arResult['NavPageCount'] < 2)
    return;
?>

<nav class="pagination-area">
    <ul class="pagination justify-content-center">

        <?php foreach($arResult['pages'] as $page) : ?>       
            
            <?php if($page['type'] == 'DISABLED') : continue; ?>

            <?php elseif($page['type'] == 'PREV') : ?>
                <li><a href="<?= $page['link'] ?>"><i class="icon-arrow-left"></i></a></li>

            <?php elseif($page['type'] == 'NEXT') : ?>
                <li><a href="<?= $page['link'] ?>"><i class="icon-arrow-right"></i></a></li>

            <?php elseif($page['type'] == 'CURRENT') : ?>            
                <li><a class="current" href="#"><?= $page['text'] ?></a></li>
            
            <?php else : ?>
                <li><a href="<?= $page['link'] ?>"><?= $page['text'] ?></a></li>

            <?php endif ?>

        <?php endforeach ?>

    </ul>
</nav><!-- .pagination-area -->
