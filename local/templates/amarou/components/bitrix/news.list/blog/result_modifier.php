<?php

foreach ($arResult['ITEMS'] as &$item) {
    $categories = [];

    $db_groups = CIBlockElement::GetElementGroups($item['ID'], true);
    while($ar_group = $db_groups->Fetch()) {
        $categories[] = [
            'title' => $ar_group['NAME'],
            'url' => $item['LIST_PAGE_URL'] . $ar_group['CODE'] . '/',
        ];
    }
    $item['categories'] = $categories;
}
