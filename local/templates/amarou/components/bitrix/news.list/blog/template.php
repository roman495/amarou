<?php
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)
    die();

use \Bitrix\Main\Localization\Loc;
Loc::loadLanguageFile(__FILE__);
?>

<section class="blog-grid">
    <div class="container">
        <?php if(!empty($arResult['ITEMS'])) : ?>
            <div class="row">
                <?php foreach ($arResult['ITEMS'] as $item) : ?>
                    <!-- Blog Item #1 -->
                    <div class="col-sm-12 col-md-6 col-lg-4">
                        <div class="post-item">
                            <div class="post-item__img">
                                <a href="<?= $item['DETAIL_PAGE_URL'] ?>">
                                    <img src="<?= $item['PREVIEW_PICTURE']['UNSAFE_SRC'] ?>" alt="blog image">
                                </a>
                            </div><!-- /.blog-img -->
                            <div class="post-item__content">
                            <div class="post-item__meta d-flex flex-wrap align-items-center">
                                <span class="post-item__meta-date"><?= date('d F Y', strtotime($item['TIMESTAMP_X'])) ?></span>
                                <div class="post-item__meta-cat">
                                    <?php foreach($item['categories'] as $cat) : ?>
                                        <a href="<?= $cat['url'] ?>"><?= $cat['title'] ?></a>
                                    <?php endforeach ?>
                                </div><!-- /.blog-meta-cat -->
                            </div><!-- /.blog-meta -->
                            <h4 class="post-item__title">
                                <a href="<?= $item['DETAIL_PAGE_URL'] ?>"><?= $item['NAME'] ?></a>
                            </h4>
                            <p class="post-item__desc"><?= $item['PREVIEW_TEXT'] ?></p>
                            <a href="<?= $item['DETAIL_PAGE_URL'] ?>" class="btn btn__secondary btn__link">
                                <i class="icon-arrow-right"></i>
                                <span><?= Loc::getMessage('READ_MORE') ?></span>
                            </a>
                            </div><!-- /.blog-content -->
                        </div><!-- /.post-item -->
                    </div><!-- /.col-lg-4 -->
                <?php endforeach ?>
            </div><!-- /.row -->

            <div class="row">
                <div class="col-sm-12 col-md-12 col-lg-12 text-center">
                    <?= $arResult['NAV_STRING'] ?>
                </div><!-- /.col-lg-12 -->
            </div><!-- /.row -->
        <?php else : ?>
            <div class="row">
                <div class="col-sm-12 text-center">
                    <p><?= Loc::getMessage('NO_POSTS') ?></p>
                </div>
            </div>
        <?php endif ?>
    </div><!-- /.container -->
</section><!-- /.blog Grid -->
