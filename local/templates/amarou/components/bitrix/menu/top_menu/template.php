<?php
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
?>
<div class="collapse navbar-collapse" id="mainNavigation">
    <ul class="navbar-nav ml-auto">

        <?php foreach ($arResult as $key => $item): ?>

            <li class="nav__item with-dropdown">

                <?php if ( $item['SELECTED']) : ?>
                    <a href="<?=$item['LINK']?>" class="dropdown-toggle nav__item-link active">
                        <?=$item['TEXT']?>
                    </a>
                <?php else : ?>
                    <a href="<?=$item['LINK']?>" class="dropdown-toggle nav__item-link">
                        <?=$item['TEXT']?>
                    </a>
                <?php endif ?>
                
                <?php if ($item['IS_PARENT']): ?>
                    
                    <ul class="dropdown-menu">
                    
                        <?php foreach ($item['subitems'] as $subitem): ?>
                        
                            <li class="nav__item">
                                <a href="<?=$subitem['LINK']?>" class="nav__item-link">
                                    <?=$subitem['TEXT']?>
                                </a>
                            </li>
                        <?php endforeach ?>
                    
                    </ul><!-- /.dropdown-menu -->
                
                <?php endif ?>
            
            </li><!-- /.nav-item -->
        
        <?php endforeach ?>

    </ul><!-- /.navbar-nav -->
</div><!-- /.navbar-collapse -->
