<?php
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

if(empty($arResult))
	return '';

global $APPLICATION;

$output = <<<START
<section class="page-title page-title-layout5 bg-overlay bg-parallax">
    <div class="bg-img">
        <img src="<?= SITE_TEMPLATE_PAGE ?>/assets/images/page-titles/6.jpg" alt="background">
    </div>
    <div class="container">
        <div class="row">
            <div class="col-sm-12 col-md-12 col-lg-8 col-xl-4">
                <h1 class="pagetitle__heading">{$APPLICATION->GetTitle()}</h1>
                <nav>
                    <ol class="breadcrumb mb-0">
START;

foreach ($arResult as $value) {
    if ($value != end($arResult)) {
        $breadcrumb_item = '<li class="breadcrumb-item">';
        $breadcrumb_item .= '<a href="' . $value['LINK'] . '">' . $value['TITLE'] . '</a>';
        $breadcrumb_item .= '</li>';    
    } else {
        $breadcrumb_item = '<li class="breadcrumb-item active" aria-current="page">';
        $breadcrumb_item .= $value['TITLE'];
        $breadcrumb_item .= '</li>';
    }

    $output .= $breadcrumb_item;
}

$output .= <<<END
                    </ol>
                </nav>
            </div><!-- /.col-xl-4 -->
        </div><!-- /.row -->
    </div><!-- /.container -->
</section><!-- /.page-title -->
END;

return $output;