<?php
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
?>

    <!-- ========================
      Footer
    ========================== -->
    <footer class="footer">
      <div class="footer-primary">
        <div class="container">
          <div class="row">
            <div class="col-sm-12 col-md-6 col-lg-3 col-xl-5 footer-widget footer-widget-about">
              <h6 class="footer-widget-title">About Amarou</h6>
              <div class="footer-widget-content">
                <p class="mb-20">A leading developer of A-grade commercial, industrial and residential projects in USA.
                  Since its foundation the company has doubled its turnover year on year with its staff.</p>
                <a href="request-quote.html" class="btn btn__primary btn__link ">
                  <i class="icon-arrow-right"></i><span>Request A Quote</span>
                </a>
              </div>
            </div><!-- /.col-xl-4 -->
            <div class="col-6 col-sm-6 col-md-6 col-lg-2 col-xl-2 footer-widget footer-widget-nav">
              <h6 class="footer-widget-title">Services</h6>
              <div class="footer-widget-content">
                <nav>
                  <ul class="list-unstyled">
                    <li><a href="#">Construction Manage</a></li>
                    <li><a href="#">Construction Consultants</a></li>
                    <li><a href="#">Architecture & Building</a></li>
                    <li><a href="#">Home Renovations</a></li>
                    <li><a href="#">Tiling & Painiting</a></li>
                    <li><a href="#">Interior Design</a></li>
                  </ul>
                </nav>
              </div><!-- /.footer-widget-content -->
            </div><!-- /.col-xl-2 -->
            <div class="col-6 col-sm-6 col-md-6 col-lg-2 col-xl-2 footer-widget footer-widget-nav">
              <h6 class="footer-widget-title">Company</h6>
              <div class="footer-widget-content">
                <nav>
                  <ul class="list-unstyled">
                    <li><a href="about-us.html">About Us</a></li>
                    <li><a href="leadership-team.html">Meet Our Team</a></li>
                    <li><a href="blog.html">News & Media</a></li>
                    <li><a href="projects-grid.html">Case Studies</a></li>
                    <li><a href="contacs.html">Contacts</a></li>
                    <li><a href="careers.html">Careers</a></li>
                  </ul>
                </nav>
              </div><!-- /.footer-widget-content -->
            </div><!-- /.col-xl-2 -->
            <div class="col-sm-12 col-md-6 col-lg-4 col-xl-3 footer-widget footer-widget-contact">
              <h6 class="footer-widget-title">Quick Contact</h6>
              <div class="footer-widget-content">
                <p class="mb-20">If you have any questions or need help, feel free to contact with our team.</p>
                <a class="contact-number contact-number-white d-flex align-items-center mb-20" href="tel:5565454117">
                  <i class="icon-phone"></i><span>(002) 55 654 541 17</span>
                </a><!-- /.contact__numbr -->
                <p class="mb-30">2307 Beverley Rd Brooklyn, New York 11226 United States.</p>
                <ul class="social-icons list-unstyled">
                  <li><a href="#"><i class="fab fa-facebook-f"></i></a></li>
                  <li><a href="#"><i class="fab fa-instagram"></i></a></li>
                  <li><a href="#"><i class="fab fa-twitter"></i></a></li>
                </ul><!-- /.social-icons -->
              </div><!-- /.footer-widget-content -->
            </div><!-- /.col-xl-4 -->
          </div><!-- /.row -->
        </div><!-- /.container -->
      </div><!-- /.footer-primary -->
      <div class="footer-copyrights">
        <div class="container">
          <div class="row">
            <div class="col-sm-12 col-md-12 col-lg-12 text-center">
              <p class="mb-0"> &copy; 2020 Amarou. With Love by
                <a href="http://themeforest.net/user/7oroof">7oroof.com</a>
              </p>
            </div><!-- /.col-lg-12 -->
          </div><!-- /.row -->
        </div><!-- /.container -->
      </div><!-- /.footer-copyrights-->
    </footer><!-- /.Footer -->
    <button id="scrollTopBtn"><i class="fas fa-long-arrow-alt-up"></i></button>

    <div class="search-popup">
      <i class="search-popup__close">&times;</i>
      <form class="search-popup__form">
        <input type="text" class="search-popup__form__input" placeholder="Type Words Then Enter">
        <button class="search-popup__btn"><i class="fas fa-search"></i></button>
      </form>
    </div><!-- /. search-popup -->

  </div><!-- /.wrapper -->
</body>
</html>
