<?php

function dd($data)
{
    $result = <<<TEXT
    <div id='php-dump'>
        <style type="text/css" scoped>
            pre {
                font-size: 16px;
                font-family: 'JetBrains Mono';
                background: #263238;
                color: #388E3C;
                margin: 15px auto;
                padding: 15px;
                border-radius: 5px;
            }
        </style>
TEXT;

    $result .= '<pre>';
    $result .= print_r($data, 1);
    $result .= '</pre>';

    echo $result . '</div>';
}
