<?php

$aMenuLinks = [
    [
        'About Us',
        '/company/about-us/',
        [],
        [],
        ''
    ],
    [
        'Core Values',
        '/company/core-values/',
        [],
        [],
        ''
    ],
    [
        'Leadersheep Team',
        '/company/leadersheep-team/',
        [],
        [],
        ''
    ],
    [
        'Pricing & Plans',
        '/company/pricing-n-plans/',
        [],
        [],
        ''
    ],
    [
        'Help & FAQs',
        '/company/help-n-faqs/',
        [],
        [],
        ''
    ],
    [
        'Careers',
        '/company/careers/',
        [],
        [],
        ''
    ],
];